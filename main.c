#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */
long int *fac_sum;
int arrSize = 1;
int multiple_10 = 0;

void fac_cal(long int val)
{
    int i,j;
    int carry;
    
    j=arrSize;
    if(val==1)
    {
        fac_sum[0]=val;
    }
    else
    {
        for(i=0;i<j;i++)
        {
            fac_sum[i]*=val;
        }
    }
    for(i=0;i<arrSize;i++)
    {
        if(fac_sum[i]>=10)
        {
            carry = fac_sum[i]/10;
            fac_sum[i]=fac_sum[i]%10;
            if(i==(arrSize-1))
            {
                arrSize+=1;
                fac_sum = realloc( fac_sum, arrSize * sizeof(long int) );
                fac_sum[i+1]=carry;
            }
            else
            {
                fac_sum[i+1]+=carry;
            }            
        }
    }
}

int main(int argc, char *argv[]) {
    int i=0, k=0;
    int input=0;
    
    if(argc!=2)
        printf("Parameter missing!\n");
    else
    {
        input=atoi(argv[1]);
        printf("%s: %d, %d\n", __FILE__, argc, input);
    }
        
    fac_sum = (long int*) malloc(arrSize * sizeof(long int));
        
    for(i=1;i<=input;i++)
    {
        fac_cal(i);
        printf(".");
        if(i%50==0)
            printf("%d\n", i);
    }
    printf("\n");
    
    for(i=1;i<=arrSize;i++)
    {
        printf("%d", fac_sum[arrSize-i]);
    }
    printf("\n\n");
    
    free(fac_sum);
	return 0;
}
